const http = require('http')

const port =  3000

const server = http.createServer(function(request,response){
	//You can use request.url to get the current destination of the user in the browser. You can then check if the current destination of the user matches with any endpoint and if it does, do the respective processes for each endpoint
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the login page')
		} else { //If none of the endpoints match the current destination of the user, return a default value like 'Page not available'
			response.writeHead(404, {'Content-Type': 'text/plain'})
			response.end("I'm sorry the page you are looking for cannot be found")
		}
})

server.listen(port)

console.log(`Server now accessible at localhost:${port}`)