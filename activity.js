// 1. What directive is used by Node.js in loading the modules it needs?
// Write answers here...
let http = require ('http')

// 2. What Node.js module contains a method for server creation?

Declare variable for the server creation like ('port')

// 3. What is the method of the http object responsible for creating a server using Node.js?

Declaration of "http.createServer(function(request,response)"

// 4. What method of the response object allows us to set status codes and content types?
	
That is the header with codes like response.writeHead

// 5. Where will console.log() output its contents when run in Node.js?

To the "port" value assigned

// 6. What property of the request object contains the address's endpoint?

Next to the response.end which is the listen(port)